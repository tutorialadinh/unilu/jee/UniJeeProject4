package lu.uni.example.bean;

import lombok.Getter;
import lombok.Setter;
import lu.uni.example.service.JsonParserLocal;
import org.apache.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;


import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.Pattern;
import javax.ws.rs.core.MediaType;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;

@ManagedBean
@SessionScoped
public class IndexBean implements Serializable {
    private static final Logger logger = Logger.getLogger(IndexBean.class);
    private static final String NO_DATA = "NO DATA FOUND";

    @Getter
    @Setter
    // Allow numbers, ',' , '-' , empty String *
    @Pattern(regexp = "^[0-9\\,\\-]*$", message = "Allowed format : '2015,2016,2015-2017'. NO SPACE allowed.")
    private String inputTextForYear;

    @Getter
    @Setter
    private String outputJson;

    @Getter
    @Setter
    private String listOfSelectedYears;

    @Getter
    @Setter
    @EJB
    private JsonParserLocal jsonParserLocal;


    /**
     * Method executed at startup
     */
    @PostConstruct
    public void init() {
        this.outputJson = "";
        this.listOfSelectedYears = "";
    }


    // =============================================================================================================================

    public void onKeyInputEvent() {
        logger.info("ON KEY INPUT input : " + this.inputTextForYear);
        logger.info("ON KEY INPUT output : " + this.outputJson);

        init();
    }

    public void getResult() throws InterruptedException {
        // Get JSON response
        logger.info("SELECTED YEARS : " + this.inputTextForYear);
        this.outputJson = this.jsonParserLocal.getResult(this.inputTextForYear);

        // Display the list of selected years
        this.listOfSelectedYears = this.jsonParserLocal.getListOfYears();

        // Display message
        showInfo();
    }

    public StreamedContent downloadFile() {
        logger.info("JSON TO Download : " + this.outputJson);
        InputStream stream = new ByteArrayInputStream(this.outputJson.getBytes());

        return DefaultStreamedContent.builder()
                .name("statec.json")
                .contentType(MediaType.APPLICATION_JSON)
                .stream(() -> stream)
                .build();
    }

    // =============================================================================================================================

    private void showInfo() {
        logger.info("JSON showInfo : " + this.outputJson);

        if (this.outputJson.equals(NO_DATA) || this.outputJson.isEmpty())
            addMessage(FacesMessage.SEVERITY_ERROR, "No Json", "The Download button will be disabled");
        else
            addMessage(FacesMessage.SEVERITY_INFO, "JSON created", "The Download button is now enabled");
    }

    private void addMessage(FacesMessage.Severity severity, String summary, String detail) {
        FacesContext.getCurrentInstance().
                addMessage(null, new FacesMessage(severity, summary, detail));
    }
}