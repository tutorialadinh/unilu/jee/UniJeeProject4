package lu.uni.example.model;

import lombok.Getter;
import lombok.Setter;

import javax.json.bind.annotation.JsonbProperty;


public class StatecEntity {

    @Getter
    @Setter
    @JsonbProperty("date")
    private String dateValue;

    @Getter
    @Setter
    @JsonbProperty("resident borderers")
    private String residentBorderers;

    @Getter
    @Setter
    @JsonbProperty("non-resident borderers")
    private String notResidentBorderers;

    @Getter
    @Setter
    @JsonbProperty("national wage-earners")
    private String nationalWageEarners;

    @Getter
    @Setter
    @JsonbProperty("domestic wage-earners")
    private String domesticWageEarners;

    @Getter
    @Setter
    @JsonbProperty("national self-employment")
    private String nationalSelfEmployment;

    @Getter
    @Setter
    @JsonbProperty("domestic self-employment")
    private String domesticSelfEmployment;

    @Getter
    @Setter
    @JsonbProperty("national employment")
    private String nationalEmployment;

    @Getter
    @Setter
    @JsonbProperty("domestic employment")
    private String domesticEmployment;

    @Getter
    @Setter
    @JsonbProperty("number of unemployed")
    private String numberOfUnemployed;

    @Getter
    @Setter
    @JsonbProperty("active population")
    private String activePopulation;
}
