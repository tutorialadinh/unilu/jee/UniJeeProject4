package lu.uni.example.service;

import javax.ejb.Local;

@Local
public interface JsonParserLocal {
    String getResult(String selectedYears);

    String getListOfYears();
}
