package lu.uni.example.service;


import lombok.Getter;
import lombok.Setter;
import lu.uni.example.model.StatecEntity;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.ejb.Stateless;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyOrderStrategy;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

@Stateless
public class JsonParser implements JsonParserLocal {
    private static final Logger logger = Logger.getLogger(JsonParserLocal.class);
    private static final String NO_DATA = "NO DATA FOUND";

    @Getter
    @Setter
    private TreeSet<Integer> setOfSelectedYears;

    @Getter
    @Setter
    private List<StatecEntity> listOfStatecEntity;

    @Override
    public String getResult(String selectedYears) {
        // Reinitialize the Arraylist at each request
        setOfSelectedYears = new TreeSet<>();
        listOfStatecEntity = new ArrayList<>();

        // Store the list of Year
        parseYearInputText(selectedYears);

        // Get XML content from resources
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("statec.xml");
        assert is != null;
        readXML(is);

        // Not sorting JSON keys name alphabetically + format output
        JsonbConfig config = new JsonbConfig().withPropertyOrderStrategy(PropertyOrderStrategy.ANY).withFormatting(true);
        Jsonb jsonb = JsonbBuilder.create(config);

        if (listOfStatecEntity.size() != 0)
            return jsonb.toJson(listOfStatecEntity);
        else
            return NO_DATA;
    }

    @Override
    public String getListOfYears() {
        StringBuilder listOfYears = new StringBuilder();
        listOfYears.append("List of selected years : ");

        for (Integer year : setOfSelectedYears) {
            listOfYears.append(year).append("; ");
        }
        return listOfYears.toString();
    }


    // ====================================================================================================================================

    private void parseYearInputText(String selectedYear) {
        // Check if String contains ',' before splitting
        if (selectedYear.contains(",")) {
            // If we have a set of year 2009-2015; 2017, first split by ","
            String[] yearList = selectedYear.split(",");

            // Check if there is a '-'
            for (String inputYear : yearList) {
                // If the String doesn't contain "-"
                if (!inputYear.contains("-")) {
                    setOfSelectedYears.add(Integer.parseInt(inputYear));
                } else {
                    String[] yearRangeList = inputYear.split("-");

                    // Calculate the difference in year
                    int years = Integer.parseInt(yearRangeList[1]) - Integer.parseInt(yearRangeList[0]);

                    for (int x = 0; x <= years; x++) {
                        setOfSelectedYears.add(Integer.parseInt(yearRangeList[0]) + x);
                    }
                }
            }

        } else if (selectedYear.contains("-")) {
            // Check if there is already a '-'
            String[] yeaRangeList = selectedYear.split("-");

            // Calculate the difference in year
            int years = Integer.parseInt(yeaRangeList[1]) - Integer.parseInt(yeaRangeList[0]);

            for (int y = 0; y <= years; y++) {
                setOfSelectedYears.add(Integer.parseInt(yeaRangeList[0]) + y);
            }

        } else {
            // It means we added only one year (if we have correctly set the Regex in IndexBean.java)
            setOfSelectedYears.add(Integer.parseInt(selectedYear));
        }
    }

    private void readXML(InputStream is) {
        boolean dateFound = false;
        Document doc = null;
        try {
            doc = parseXML(is);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            logger.error("STATEC XML file not found or can't be read : ", e);
        }

        if (doc != null) {

            // Get the list of "Row" where they stored the Date and the value
            NodeList rowNodeList = doc.getElementsByTagName("Row");

            for (int k = 0; k < rowNodeList.getLength(); k++) {
                // Check its children ("RowLabels" and "Cells")
                Node nNode = rowNodeList.item(k);

                // New instance of StatecEntity
                StatecEntity statecEntity = new StatecEntity();

                for (int j = 0; j < nNode.getChildNodes().getLength(); j++) {
                    // If getNodeName() = "#text" (indent), we ignore
                    if (!nNode.getChildNodes().item(j).getNodeName().equals("#text")) {

                        if (nNode.getChildNodes().item(j).getNodeName().equals("RowLabels")) {
                            // Date
                            Element eElement = (Element) nNode;
                            Element contentElement = (Element) eElement.getElementsByTagName("RowLabel").item(0);

                            for (int yearToRetrieve : setOfSelectedYears) {
                                // Extract numbers
                                String digits = contentElement.getTextContent().replaceAll("\\D+", "").trim();

                                if (digits.equals(String.valueOf(yearToRetrieve))) {
                                    // Storing Date into entity
                                    statecEntity.setDateValue(contentElement.getTextContent());
                                    dateFound = true;

                                    break;
                                }
                            }
                        }

                        if (dateFound) {
                            // Then get the other values
                            if (nNode.getChildNodes().item(j).getNodeName().equals("Cells")) {
                                // Cell List
                                Element eElement2 = (Element) nNode;
                                NodeList cellNodeList = eElement2.getElementsByTagName("C");

                                // Storing C value into entity
                                statecEntity.setResidentBorderers(cellNodeList.item(0).getAttributes().getNamedItem("v").getNodeValue());
                                statecEntity.setNotResidentBorderers(cellNodeList.item(1).getAttributes().getNamedItem("v").getNodeValue());
                                statecEntity.setNationalWageEarners(cellNodeList.item(2).getAttributes().getNamedItem("v").getNodeValue());
                                statecEntity.setDomesticWageEarners(cellNodeList.item(3).getAttributes().getNamedItem("v").getNodeValue());
                                statecEntity.setNationalSelfEmployment(cellNodeList.item(4).getAttributes().getNamedItem("v").getNodeValue());
                                statecEntity.setDomesticSelfEmployment(cellNodeList.item(5).getAttributes().getNamedItem("v").getNodeValue());
                                statecEntity.setNationalEmployment(cellNodeList.item(6).getAttributes().getNamedItem("v").getNodeValue());
                                statecEntity.setDomesticEmployment(cellNodeList.item(7).getAttributes().getNamedItem("v").getNodeValue());
                                statecEntity.setNumberOfUnemployed(cellNodeList.item(8).getAttributes().getNamedItem("v").getNodeValue());
                                statecEntity.setActivePopulation(cellNodeList.item(9).getAttributes().getNamedItem("v").getNodeValue());

                                // Adding into the list of statecEntity
                                listOfStatecEntity.add(statecEntity);

                                // Reinitialize boolean
                                dateFound = false;
                            }
                        }
                    }
                }
            }

        }
    }

    private Document parseXML(InputStream is) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(is);
        doc.getDocumentElement().normalize();
        return doc;
    }
}
