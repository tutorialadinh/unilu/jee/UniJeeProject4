Write an EJB that can read statistical data in XML format given in this file about employment in Luxembourg taken from this Statec site. 

https://statistiques.public.lu/stat/TableViewer/tableView.aspx?ReportId=12948&sCS_ChosenLang=fr

Note that the format of the XML file is a bit weird, and you might have to "investigate" 
on the format with the help of the provided website to understand how data is stored in that XML file.

Then write a view where the users can enter a set of years in the format "2012,2013,2015" or "2010-2018"; 

the backend should then read the data from the XML file and output the data in JSON format 
as text on screen (you can choose the keys on your own).

There should also be a link provided on the view where the same result data can be downloaded as file (in the same JSON format).
